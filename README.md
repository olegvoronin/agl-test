# README #

IRL I would want to set this up with webpack, modules and a test runner.

Instead I was more interested in getting this accessible as easily as possible without need for local env setup for review.

This is published on [bitbucket.io](https://olegvoronin.bitbucket.io/agl-test/) with [tests running, too](https://olegvoronin.bitbucket.io/agl-test/tests.html)