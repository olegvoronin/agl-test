'use strict';

//should be module exports w/ app bundled with webpack

function renderPets(_json){
	var html='';
    Object.keys(_json).forEach(function(_gender){
    	html+='<h5>'+_gender+'</h5>';
        
        var pets = _json[_gender];
        
        if (pets) {
            html+='<ul>';
            pets.forEach(function(_pet){
            	html+='<li>'+_pet+'</li>';
            });
            html+='</ul>';
        }
    });
	return html;
}


function groupPets(_source){

  var merge = {};

  _source.forEach(function(_owner){
  	//make sure the owner gender is set and isn't
  	var ownerGender = !{}[_owner.gender] && _owner.gender || 'Unspecified';
    
    merge[ownerGender] = merge[ownerGender] || [];

    if (_owner.hasOwnProperty('pets') && _owner.pets) {
      var ownerCats = _owner.pets.filter(function(_pet){
        return _pet.type === 'Cat';
      });
      
      ownerCats.forEach(function(_cat){
      	var catName = _cat.name || 'Grumpy';
        merge[ownerGender].push(catName);
      });
    }
  });

  //sort alphabetically
  Object.keys(merge).forEach(function(_key){
    merge[_key].sort();
  });
	//console.info(merge);
  return merge;
}
