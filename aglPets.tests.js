'use strict';

var expect = chai.expect;

describe("groupPets", function() {  
  it("should work with empty response", function(){
    var testResponse = [];
  	expect(groupPets(testResponse)).to.deep.equal({});
  });
  
  it("should process owners without specified name", function(){
    var testResponse = [{"gender": "Male", pets: [{"name": "Tom", "type": "Cat"}]}];
  	expect(groupPets(testResponse)).to.deep.equal({ "Male": ["Tom"]});
  });
  
  it("should process owners without specified gender", function(){
    var testResponse = [{"name": "Nick", pets: [{"name": "Tom", "type": "Cat"}]}];
  	expect(groupPets(testResponse)).to.deep.equal({ "Unspecified": ["Tom"]});
  });
  
  it("should process pets without specified name", function(){
    var testResponse = [{"name": "Nick", "gender": "Male", pets: [{"type": "Cat"}]}];
  	expect(groupPets(testResponse)).to.deep.equal({ "Male": ["Grumpy"]});
  });
  
  it("should sort cats alphabetically", function(){
    var testResponse = [{"name": "Nick", "gender": "Male", pets: [{"name": "Tom", "type": "Cat"}, {"name": "Max", "type": "Cat"}]}];
  	expect(groupPets(testResponse)).to.deep.equal({ "Male": ["Max", "Tom"]});
  });
  
  it("should process owners with gender as object prototype property or method without breaking", function(){
    var testResponse = [{"name": "Nick", "gender": "constructor", pets: [{"name": "Tom", "type": "Cat"}]}];
  	expect(groupPets(testResponse)).to.deep.equal({ "Unspecified": ["Tom"]});
  });
  
  it("should work for the test example", function(){
  	expect(groupPets(_json)).to.deep.equal({"Male":["Garfield","Jim","Max","Tom"],"Female":["Garfield","Simba","Tabby"]});
  });
});